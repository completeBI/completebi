package completeBI.controller.dashboard;

import completeBI.model.deshboard.Sample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
@RequestMapping("/controller")
@Api(value = "Sample", description = "Operations Sample Demo")
public class SampleController {

    private static final Logger logger = LogManager.getLogger(SampleController.class);

    @ApiOperation(value = "View a list of available sample")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public List<Sample> list() {

        logger.info("calling sample list");

        Sample s = new Sample();
        s.setName1("Test");
        s.setName2("Test1");

        List<Sample> l = new ArrayList<>();
        l.add(s);

        logger.info("Return sample list");
        return l;
    }

}
