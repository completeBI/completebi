package completeBI.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ComponentScan(basePackages = {"completeBI.*"})
public class CompleteBiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompleteBiApplication.class, args);
	}

}
